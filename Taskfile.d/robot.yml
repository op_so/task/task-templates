---
# https://taskfile.dev
# @description: A set of tasks for Robot Framework projects.
# @tags: robot-framework, docker, CI, CLI
# @authors: FX Soubirou <soubirou@yahoo.fr>
# @file-raw: https://gitlab.com/op_so/task/task-templates/-/raw/main/Taskfile.d/robot.yml
# @file-ui: https://gitlab.com/op_so/task/task-templates/-/blob/main/Taskfile.d/robot.yml
# @home: https://gitlab.com/op_so/task/task-templates
# @links: [Robot Framework](https://robotframework.org/)
# @license: MIT
# @status: stable
# @deprecated-tasks:

version: '3'

vars:
  IMAGE_ROBOT: jfxs/robot-framework  # Default Robot Framework image

tasks:

  badge:
    desc: 'Create a badge from the results of the tests. Arguments: FILE|F=output.xml [DIR|D=.] [STYLE|S=plastic] (*)'
    summary: |
      [ROBOT] Create a badge all_tests.svg from the results of the Robot Framework tests with shields.io service
      Usage: task robot:badge FILE|F=<output_file_path> [DIR|D=<badge_directory>] [STYLE|S=<badge_style>]

      Arguments:
        FILE  | F: Output XML results file (required)
        DIR   | D: Generated badge directory (optional, current directory by default)
        STYLE | S: Docker Robot Framework image to use (optional, by default {{.IMAGE_ROBOT}})

      Requirements:
        - curl
        - Internet connexion
    vars:
      DIR: '{{.DIR | default .D | default "."}}'
      FILE: '{{.FILE | default .F}}'
      STYLE: '{{.STYLE | default .S | default "for-the-badge"}}'
      GREEN: green
      ORANGE: orange
      RED: red
    cmds:
      - |
        line_all=$(sed -n -e '/<total>/,/<\/total>/p' "{{.FILE}}" | grep All)
        all_fail=$(echo "$line_all" | sed 's/.*fail="\([^"]*\).*/\1/')
        all_skip=$(echo "$line_all" | sed 's/.*skip="\([^"]*\).*/\1/')
        all_pass=$(echo "$line_all" | sed 's/.*pass="\([^"]*\).*/\1/')
        all_total=$((all_fail + all_skip + all_pass))
        echo "Passed Tests: $all_pass / $all_total (Skipped tests $all_skip)"
        if [ "$all_fail" -gt "0" ]; then
          color="{{.RED}}"
        elif [ "$all_skip" -gt "0" ]; then
          color="{{.ORANGE}}"
        else
          color="{{.GREEN}}"
        fi
        mkdir -p "{{.DIR}}"
        curl -s --output "{{.DIR}}/all_tests.svg"  "https://img.shields.io/badge/All%20Tests-${all_pass}%20%2F%20${all_total}%20passed-${color}?style={{.STYLE}}&logo=robotframework"
    preconditions:
      - sh: test -n "{{.FILE}}"
        msg: "FILE|F argument is required"
      - sh: test -f "{{.FILE}}"
        msg: "File {{.FILE}} not found!"
    silent: true

  cop:
    desc: 'Static code analysis of Robot Framework language with Robocop. Arguments: DIR|D=tests [ARG|A="--check"] [IMG|I=jfxs/robot-framework:latest] [PULL|P=<n|N>] (*)'
    summary: |
      [ROBOT] Static code analysis of Robot Framework language with Robocop.
      Usage: task robot:cop DIR|D=<tests_dir> [ARG|A="cli_arguments"] [IMG|I=<docker_image:tag>] [PULL|P=<n|N>]

      Arguments:
        DIR  | D: Directory or file to format (required)
        ARG  | A: Arguments and options (optional)
        IMG  | I: Docker Robot Framework image to use (optional, by default {{.IMAGE_ROBOT}})
        PULL | P: Pull docker image (optional, by default yes)

      Requirements:
        - robocop or docker
    vars:
      DIR: '{{.DIR | default .D}}'
      ARG: '{{.ARG | default .A}}'
      IMG: '{{.IMG | default .I}}'
      PULL: '{{.PULL | default .P}}'
      CMD: "robocop {{.ARG}} {{.DIR}}"
    cmds:
      - task: exec-local-docker-cmd
        vars: {CMD_EXEC: "robocop", CMD: "{{.CMD}}", IMG: "{{.IMG}}", PULL: "{{.PULL}}"}
    preconditions:
      - sh: test -n "{{.DIR}}"
        msg: "DIR|D argument is required"
      - sh: command -v docker || command -v robocop
        msg: "robocop or docker are not installed"
    silent: true

  mask:
    desc: 'Mask sensitive data of a file. Arguments: FILE|F=file.txt DATA|D=paswword1,password2 (*)'
    summary: |
      [ROBOT] Mask sensitive data of a file.
      Usage: task robot:mask FILE|F=<file_path> DATA|D=paswword1,password2

      Arguments:
        FILE | F: Fix files (required)
        DATA | D: Data to mask separated by comma (required)
    vars:
      DATA: '{{.DATA | default .D}}'
      FILE: '{{.FILE | default .F}}'
      MASK: '\*\*MASKED\*\*'
    cmds:
      - |
        data_index=0
        for data in $(echo "{{.DATA}}" | tr ',' '\n')
        do
          count_init=$(grep -o "{{.MASK}}" "{{.FILE}}" | wc -l)
          data_escaped=$(echo "$data" | sed -e 's/[]\/$*.^[]/\\&/g')
          sed -i.bu -e s/"$data_escaped"/"{{.MASK}}"/g "{{.FILE}}"
          count=$(grep -o "{{.MASK}}" "{{.FILE}}" | wc -l)
          delta=$((count - count_init))
          echo "Masked variable [$data_index]: $delta matches"
          data_index=$((data_index + 1))
        done
      - defer: rm -f "{{.FILE}}.bu"
    preconditions:
      - sh: test -n "{{.DATA}}"
        msg: "DATA|D argument is required"
      - sh: test -n "{{.FILE}}"
        msg: "FILE|F argument is required"
      - sh: test -f "{{.FILE}}"
        msg: "File {{.FILE}} not found!"
    silent: true

  tidy:
    desc: 'Format Robot Framework files. Arguments: DIR|D=tests [ARG|A="--check"] [IMG|I=jfxs/robot-framework:latest] [PULL|P=<n|N>] (*)'
    summary: |
      [ROBOT] Format Robot Framework files with Robotidy.
      Usage: task robot:tidy DIR|D=<tests_dir> [ARG|A="cli_arguments"] [IMG|I=<docker_image:tag>] [PULL|P=<n|N>]

      Arguments:
        DIR  | D: Directory or file to format (required)
        ARG  | A: Arguments and options (optional)
        IMG  | I: Docker Robot Framework image to use (optional, by default {{.IMAGE_ROBOT}})
        PULL | P: Pull docker image (optional, by default yes)

      Requirements:
        - robotidy or docker
    vars:
      DIR: '{{.DIR | default .D}}'
      ARG: '{{.ARG | default .A}}'
      IMG: '{{.IMG | default .I}}'
      PULL: '{{.PULL | default .P}}'
      CMD: "robotidy {{.ARG}} {{.DIR}}"
    cmds:
      - task: exec-local-docker-cmd
        vars: {CMD_EXEC: "robotidy", CMD: "{{.CMD}}", IMG: "{{.IMG}}", PULL: "{{.PULL}}"}
    preconditions:
      - sh: test -n "{{.DIR}}"
        msg: "DIR|D argument is required"
      - sh: command -v docker || command -v robotidy
        msg: "robotidy or docker are not installed"
    silent: true

  exec-local-docker-cmd:
    vars:
      IMG_D: '{{.IMG | default .IMAGE_ROBOT}}'
    cmds:
      - |
        if (! test -n "{{.IMG}}") && (command -v "{{.CMD_EXEC}}" 2>&1 >/dev/null); then
          {{.CMD}}
        else
          if [ "{{.PULL}}" != "n" ] && [ "{{.PULL}}" != "N" ]; then docker pull "{{.IMG_D}}"; fi
          docker run -t --rm -v $(pwd):/tests -w /tests "{{.IMG_D}}" sh -c '{{.CMD}}'
        fi
    preconditions:
      - sh: command -v docker || command -v "{{.CMD_EXEC}}"
        msg: "CMD_EXEC or docker are not installed"
      - sh: test -n "{{.CMD}}"
        msg: "CMD argument is required"
      - sh: test -n "{{.CMD_EXEC}}"
        msg: "CMD_EXEC argument is required"
    silent: true
